﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Anagrammer.Tests
{
    [TestFixture]
    public class AnagramFinderTests
    {
        private static AnagramFinder _anagramFinder;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var wordList = ReadWordsFromResource();
            wordList.Count.Should().Be(869228);

            _anagramFinder = new AnagramFinder(wordList);
        }


        private List<string> ReadWordsFromResource()
        {
            var resourceName = "Anagrammer.Tests.dic-0294.txt";
            var fs = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            var sr = new StreamReader(fs);
            var words = new List<string>();

            string line = null;

            while ((line = sr.ReadLine()) != null)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                words.Add(line.Trim());
            }

            return words;
        }

        [Test]
        public void FindAnagrams_FromBigWordList()
        {
            var findAnagramsOf = "deposit";
            var anagrams = _anagramFinder.FindAnagrams(findAnagramsOf);

            var expectedSignature = findAnagramsOf.GetSignature();

            foreach (var anagram in anagrams)
            {
                anagram.GetSignature().Should().Be(expectedSignature);
            }
        }
    }
}