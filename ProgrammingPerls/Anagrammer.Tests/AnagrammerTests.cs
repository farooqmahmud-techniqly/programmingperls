﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Anagrammer.Tests
{
    [TestFixture]
    public class AnagrammerTests
    {
        private static List<string> _wordList = new List<string>();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _wordList = ReadWordsFromResource();
            _wordList.Count.Should().Be(869228);
        }

        private List<string> ReadWordsFromResource()
        {
            var resourceName = "Anagrammer.Tests.dic-0294.txt";
            var fs = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            var sr = new StreamReader(fs);
            var words = new List<string>();

            string line = null;

            while ((line = sr.ReadLine()) != null)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                words.Add(line.Trim());
            }

            return words;
        }

        [Test]
        public void FindAnagrams_FindsAnagrams()
        {
            var findAnagramsOf = "deposit";
            var expectedAnagrams = new[] {"deposit", "dopiest", "posited", "topside"};
            var unexpectedAnagrams = new[] {"devil", "dirt", "tomato", "zephyr"};
            var wordList = expectedAnagrams.Concat(unexpectedAnagrams);

            var anagrams = findAnagramsOf.FindAnagrams(wordList).ToArray();

            anagrams.Length.Should().Be(expectedAnagrams.Length);

            foreach (var anagram in expectedAnagrams)
            {
                Assert.IsTrue(expectedAnagrams.Contains(anagram));
                Assert.IsFalse(unexpectedAnagrams.Contains(anagram));
            }
        }

        [Test]
        public void FindAnagrams_FromBigWordList()
        {
            var findAnagramsOf = "deposit";
            var anagrams = findAnagramsOf.FindAnagrams(_wordList).ToArray();

            var expectedSignature = findAnagramsOf.GetSignature();

            foreach (var anagram in anagrams)
            {
                anagram.GetSignature().Should().Be(expectedSignature);
            }
        }
    }
}