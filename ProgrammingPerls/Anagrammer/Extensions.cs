﻿using System;
using System.Collections.Generic;

namespace Anagrammer
{
    public static class Extensions
    {
        public static IEnumerable<string> FindAnagrams(
            this string findAnagramsOf,
            IEnumerable<string> wordList)
        {
            var findAnagramsOfSignature = GetSignature(findAnagramsOf);

            foreach (var word in wordList)
            {
                var wordSignature = GetSignature(word);

                if (string.Compare(wordSignature, findAnagramsOfSignature, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    yield return word;
                }
            }
        }

        public static string GetSignature(this string word)
        {
            var chars = word.Trim().ToCharArray();
            Array.Sort(chars);
            return new string(chars);
        }
    }
}