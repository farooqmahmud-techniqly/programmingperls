﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Anagrammer
{
    public class AnagramFinder
    {
        readonly Dictionary<string, List<string>> _dictionary = new Dictionary<string, List<string>>();

        public AnagramFinder(IList<string> wordList)
        {
            ProcessWordList(wordList);
        }

        public IEnumerable<string> FindAnagrams(string word)
        {
            var signature = word.GetSignature();
            return _dictionary[signature];
        }

        private void ProcessWordList(IEnumerable<string> wordList)
        {
            foreach (var word in wordList)
            {
                var signature = word.GetSignature();

                if (_dictionary.ContainsKey(signature))
                {
                    _dictionary[signature].Add(word);
                }
                else
                {
                    _dictionary.Add(signature, new List<string>(new []{word}));
                }

            }
        }

        
    }
}