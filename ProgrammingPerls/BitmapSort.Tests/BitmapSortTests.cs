﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BitmapSort.Tests
{
    [TestFixture]
    public class BitmapSortTests
    {
        private const int DefaultArraySize = 10*1000000;

        private void AssertArrayIsSorted(int[] sortedArray)
        {
            for (var i = 0; i < sortedArray.Length; i++)
            {
                if (i == sortedArray.Length - 1)
                {
                    break;
                }

                Assert.IsTrue(sortedArray[i] <= sortedArray[i + 1]);
            }
        }

        private static int[] InitializeArray(int size, int minValue = 0)
        {
            var arrayToSort = new int[size];

            var random = new Random();

            for (var i = 0; i < size; i++)
            {
                arrayToSort[i] = random.Next(minValue, size - 1);
            }

            return arrayToSort;
        }

        [Test]
        public void BitmapSort_SortsArray()
        {
            var arrayToSort = InitializeArray(DefaultArraySize);
            var sortedArray = arrayToSort.BitmapSort();
            AssertArrayIsSorted(sortedArray.ToArray());
        }

        [Test]
        public void SortArray_UsingFramework()
        {
            var arrayToSort = InitializeArray(DefaultArraySize);
            Array.Sort(arrayToSort);
            AssertArrayIsSorted(arrayToSort);
        }
    }
}