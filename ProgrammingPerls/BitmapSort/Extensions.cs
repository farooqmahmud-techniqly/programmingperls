﻿using System.Collections.Generic;

namespace BitmapSort
{
    public static class Extensions
    {
        public static IEnumerable<int> BitmapSort(this int[] arrayToSort)
        {
            var bitmap = new int[arrayToSort.Length];

            BitmapSortInternal(arrayToSort, bitmap);

            foreach (var i in OutputBitmap(bitmap))
            {
                yield return i;
            }
        }

        private static IEnumerable<int> OutputBitmap(int[] bitmap)
        {
            foreach (var i in bitmap)
            {
                if (i == 1)
                {
                    yield return i;
                }
            }
        }

        private static void BitmapSortInternal(int[] arrayToSort, int[] bitmap)
        {
            foreach (var i in arrayToSort)
            {
                bitmap[i] = 1;
            }
        }
    }
}